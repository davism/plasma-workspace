# translation of plasma_runner_sessions.po to Slovak
# Peter Mihalik <udavac@inmail.sk>, 2009.
# Michal Sulek <misurel@gmail.com>, 2009.
# Mthw <jari_45@hotmail.com>, 2018.
# Dusan Kazik <prescott66@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-02 00:48+0000\n"
"PO-Revision-Date: 2020-03-25 12:12+0100\n"
"Last-Translator: Dusan Kazik <prescott66@gmail.com>\n"
"Language-Team: sk\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: sessionrunner.cpp:26
#, fuzzy, kde-format
#| msgid "log out"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to log out of the "
"session"
msgid "logout;log out"
msgstr "odhlásiť"

#: sessionrunner.cpp:29
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Odhlási a ukončí aktuálne sedenie pracovnej plochy"

#: sessionrunner.cpp:36
#, fuzzy, kde-format
#| msgctxt "shut down computer command"
#| msgid "shut down"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to shut down the "
"computer"
msgid "shutdown;shut down"
msgstr "vypnut"

#: sessionrunner.cpp:39
#, kde-format
msgid "Turns off the computer"
msgstr "Vypne počítač"

#: sessionrunner.cpp:46
#, fuzzy, kde-format
#| msgctxt "restart computer command"
#| msgid "restart"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to restart the "
"computer"
msgid "restart;reboot"
msgstr "restart"

#: sessionrunner.cpp:49
#, kde-format
msgid "Reboots the computer"
msgstr "Reštartuje počítač"

#: sessionrunner.cpp:57
#, fuzzy, kde-format
#| msgid "Lock the screen"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to lock the screen"
msgid "lock;lock screen"
msgstr "Zamknúť obrazovku"

#: sessionrunner.cpp:59
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Zamkne aktuálne sedenia a spustí šetrič obrazovky"

#: sessionrunner.cpp:66
#, fuzzy, kde-format
#| msgid "new session"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to save the "
"desktop session"
msgid "save;save session"
msgstr "nove sedenie"

#: sessionrunner.cpp:69
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:76
#, fuzzy, kde-format
#| msgid "switch user"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to switch user "
"sessions"
msgid "switch user;new session"
msgstr "prepnut uzivatela"

#: sessionrunner.cpp:79
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Spustí nové sedenie ako iný používateľ"

#: sessionrunner.cpp:86
#, fuzzy, kde-format
#| msgctxt "User sessions"
#| msgid "sessions"
msgctxt "KRunner keyword to list user sessions"
msgid "sessions"
msgstr "sedenia"

#: sessionrunner.cpp:87
#, kde-format
msgid "Lists all sessions"
msgstr "Zobrazí všetky sedenia"

#: sessionrunner.cpp:90
#, fuzzy, kde-format
#| msgctxt "switch user command"
#| msgid "switch"
msgctxt "KRunner keyword to switch user sessions"
msgid "switch"
msgstr "prepnut"

#: sessionrunner.cpp:92
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Prepne na aktívne sedenie používateľa :q:, alebo vypíše zoznam všetkých "
"aktívnych sedení, ak nie je poskytnuté :q:"

#: sessionrunner.cpp:114
#, kde-format
msgctxt "log out command"
msgid "Log Out"
msgstr ""

#: sessionrunner.cpp:124
#, fuzzy, kde-format
#| msgctxt "shut down computer command"
#| msgid "shut down"
msgctxt "turn off computer command"
msgid "Shut Down"
msgstr "vypnut"

#: sessionrunner.cpp:134
#, fuzzy, kde-format
#| msgctxt "restart computer command"
#| msgid "restart"
msgctxt "restart computer command"
msgid "Restart"
msgstr "restart"

#: sessionrunner.cpp:144
#, kde-format
msgctxt "lock screen command"
msgid "Lock"
msgstr ""

#: sessionrunner.cpp:154
#, fuzzy, kde-format
#| msgid "new session"
msgid "Save Session"
msgstr "nove sedenie"

#: sessionrunner.cpp:197
#, fuzzy, kde-format
#| msgid "switch user"
msgid "Switch User"
msgstr "prepnut uzivatela"

#: sessionrunner.cpp:274
#, kde-format
msgid "New Session"
msgstr "Nové sedenie"

#: sessionrunner.cpp:275
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type '%1')</li><li>Plasma widgets (such as the "
"application launcher)</li></ul>"
msgstr ""

#~ msgctxt "log out command"
#~ msgid "logout"
#~ msgstr "odhlasit"

#~ msgctxt "lock screen command"
#~ msgid "lock"
#~ msgstr "zamknut"

#~ msgctxt "restart computer command"
#~ msgid "reboot"
#~ msgstr "restartovat"

#~ msgctxt "switch user command"
#~ msgid "switch :q:"
#~ msgstr "prepnut :q:"

#~ msgid "new session"
#~ msgstr "nove sedenie"

#~ msgctxt "log out command"
#~ msgid "Logout"
#~ msgstr "Odhlásiť"

#~ msgid "Restart the computer"
#~ msgstr "Reštartovať počítač"

#~ msgctxt "shut down computer command"
#~ msgid "shutdown"
#~ msgstr "vypnut"

#~ msgid "Shut down the computer"
#~ msgstr "Vypnúť počítač"

#, fuzzy
#~| msgid "new session"
#~ msgid "Save the session"
#~ msgstr "nove sedenie"

#~ msgid "Warning - New Session"
#~ msgstr "Upozornenie - Nové sedenie"

#~ msgid ""
#~ "<p>You have chosen to open another desktop session.<br />The current "
#~ "session will be hidden and a new login screen will be displayed.<br />An "
#~ "F-key is assigned to each session; F%1 is usually assigned to the first "
#~ "session, F%2 to the second session and so on. You can switch between "
#~ "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~ "time. Additionally, the Plasma Panel and Desktop menus have actions for "
#~ "switching between sessions.</p>"
#~ msgstr ""
#~ "<p>Vybrali ste otvorenie ďalšieho sedenia.<br />Aktuálne sedenie bude "
#~ "skryté a objaví sa nová prihlasovacia obrazovka.<br />Ku každému sedeniu "
#~ "je priradený F-kláves, F%1 je zvyčajne priradený k prvému sedeniu, F%2 k "
#~ "druhému a tak ďalej. Medzi sedeniami môžete prepínať stlačením Ctrl, Alt "
#~ "a príslušného F-klávesu súčasne. Okrem toho panel Plasmy a ponuky plochy "
#~ "obsahujú akcie pre prepínanie medzi sedeniami.</p>"

#~ msgid "&Start New Session"
#~ msgstr "&Spustiť nové sedenie"
