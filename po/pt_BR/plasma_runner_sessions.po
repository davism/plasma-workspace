# Translation of plasma_runner_sessions.po to Brazilian Portuguese
# Copyright (C) 2008-2020 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2008, 2009, 2013, 2014, 2016, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2009, 2018, 2022.
# Aracele Torres <araceletorres@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-02 00:48+0000\n"
"PO-Revision-Date: 2022-03-24 11:59-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: sessionrunner.cpp:26
#, fuzzy, kde-format
#| msgid "log out"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to log out of the "
"session"
msgid "logout;log out"
msgstr "sair"

#: sessionrunner.cpp:29
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Sai, fechando a sessão atual da área de trabalho"

#: sessionrunner.cpp:36
#, fuzzy, kde-format
#| msgctxt "shut down computer command"
#| msgid "shut down"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to shut down the "
"computer"
msgid "shutdown;shut down"
msgstr "desligar"

#: sessionrunner.cpp:39
#, kde-format
msgid "Turns off the computer"
msgstr "Desliga o computador"

#: sessionrunner.cpp:46
#, fuzzy, kde-format
#| msgctxt "restart computer command"
#| msgid "restart"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to restart the "
"computer"
msgid "restart;reboot"
msgstr "reiniciar"

#: sessionrunner.cpp:49
#, kde-format
msgid "Reboots the computer"
msgstr "Reinicia o computador"

#: sessionrunner.cpp:57
#, fuzzy, kde-format
#| msgid "Lock the screen"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to lock the screen"
msgid "lock;lock screen"
msgstr "Bloquear tela"

#: sessionrunner.cpp:59
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Bloqueia as sessões atuais e inicia o protetor de tela"

#: sessionrunner.cpp:66
#, fuzzy, kde-format
#| msgid "new session"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to save the "
"desktop session"
msgid "save;save session"
msgstr "nova sessão"

#: sessionrunner.cpp:69
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:76
#, fuzzy, kde-format
#| msgid "switch user"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to switch user "
"sessions"
msgid "switch user;new session"
msgstr "trocar usuário"

#: sessionrunner.cpp:79
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Inicia uma nova sessão com um usuário diferente"

#: sessionrunner.cpp:86
#, fuzzy, kde-format
#| msgctxt "User sessions"
#| msgid "sessions"
msgctxt "KRunner keyword to list user sessions"
msgid "sessions"
msgstr "sessões"

#: sessionrunner.cpp:87
#, kde-format
msgid "Lists all sessions"
msgstr "Lista todas as sessões"

#: sessionrunner.cpp:90
#, fuzzy, kde-format
#| msgctxt "switch user command"
#| msgid "switch"
msgctxt "KRunner keyword to switch user sessions"
msgid "switch"
msgstr "trocar"

#: sessionrunner.cpp:92
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Alterna para a sessão ativa do usuário :q: ou lista todas as sessões ativas "
"se :q: não for fornecido"

#: sessionrunner.cpp:114
#, kde-format
msgctxt "log out command"
msgid "Log Out"
msgstr ""

#: sessionrunner.cpp:124
#, fuzzy, kde-format
#| msgctxt "shut down computer command"
#| msgid "shut down"
msgctxt "turn off computer command"
msgid "Shut Down"
msgstr "desligar"

#: sessionrunner.cpp:134
#, fuzzy, kde-format
#| msgctxt "restart computer command"
#| msgid "restart"
msgctxt "restart computer command"
msgid "Restart"
msgstr "reiniciar"

#: sessionrunner.cpp:144
#, kde-format
msgctxt "lock screen command"
msgid "Lock"
msgstr ""

#: sessionrunner.cpp:154
#, fuzzy, kde-format
#| msgid "new session"
msgid "Save Session"
msgstr "nova sessão"

#: sessionrunner.cpp:197
#, kde-format
msgid "Switch User"
msgstr "Trocar usuário"

#: sessionrunner.cpp:274
#, kde-format
msgid "New Session"
msgstr "Nova sessão"

#: sessionrunner.cpp:275
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type '%1')</li><li>Plasma widgets (such as the "
"application launcher)</li></ul>"
msgstr ""

#~ msgctxt "log out command"
#~ msgid "logout"
#~ msgstr "sair"

#~ msgctxt "lock screen command"
#~ msgid "lock"
#~ msgstr "bloquear"

#~ msgctxt "restart computer command"
#~ msgid "reboot"
#~ msgstr "reiniciar"

#~ msgctxt "switch user command"
#~ msgid "switch :q:"
#~ msgstr "trocar :q:"

#~ msgid "new session"
#~ msgstr "nova sessão"

#~ msgctxt "log out command"
#~ msgid "Logout"
#~ msgstr "Sair"

#~ msgid "Restart the computer"
#~ msgstr "Reiniciar computador"

#~ msgctxt "shut down computer command"
#~ msgid "shutdown"
#~ msgstr "desligar"

#~ msgid "Shut down the computer"
#~ msgstr "Desligar computador"

#, fuzzy
#~| msgid "new session"
#~ msgid "Save the session"
#~ msgstr "nova sessão"

#~ msgid "Warning - New Session"
#~ msgstr "Aviso - Nova sessão"

#~ msgid ""
#~ "<p>You have chosen to open another desktop session.<br />The current "
#~ "session will be hidden and a new login screen will be displayed.<br />An "
#~ "F-key is assigned to each session; F%1 is usually assigned to the first "
#~ "session, F%2 to the second session and so on. You can switch between "
#~ "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~ "time. Additionally, the Plasma Panel and Desktop menus have actions for "
#~ "switching between sessions.</p>"
#~ msgstr ""
#~ "<p>Você escolheu abrir outra sessão da área de trabalho.<br />A sessão "
#~ "atual será ocultada e será mostrada uma nova tela de autenticação.<br />É "
#~ "atribuída uma tecla de função para cada sessão, sendo a F%1 normalmente "
#~ "atribuída à primeira sessão, F%2 à segunda e assim por diante. Você pode "
#~ "trocar de sessão pressionando ao mesmo tempo Ctrl, Alt e a tecla de "
#~ "função respectiva. Adicionalmente, o painel do Plasma e os menus da área "
#~ "de trabalho têm ações para alternar entre as sessões.</p>"
