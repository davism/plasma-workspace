# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-27 00:48+0000\n"
"PO-Revision-Date: 2022-08-21 18:42+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: plugin/actionlist.cpp:86
#, kde-format
msgid "Open with:"
msgstr "Opna með:"

#: plugin/actionlist.cpp:99
#, kde-format
msgid "Properties"
msgstr "Eiginleikar"

#: plugin/actionlist.cpp:146
#, kde-format
msgid "Add to Desktop"
msgstr "Bæta á skjáborð"

#: plugin/actionlist.cpp:151
#, kde-format
msgid "Add to Panel (Widget)"
msgstr "Bæta við stiku (viðmótshluti)"

#: plugin/actionlist.cpp:156
#, kde-format
msgid "Pin to Task Manager"
msgstr "Festa í verkefnastjóra"

#: plugin/actionlist.cpp:307 plugin/rootmodel.cpp:429
#, kde-format
msgid "Recent Files"
msgstr "Nýlegar skrár"

#: plugin/actionlist.cpp:317
#, kde-format
msgid "Forget Recent Files"
msgstr "Gleyma nýlegum skrám"

#: plugin/actionlist.cpp:395
#, kde-format
msgid "Edit Application…"
msgstr "Breyta forriti…"

#: plugin/actionlist.cpp:429
#, kde-format
msgctxt "@action opens a software center with the application"
msgid "Uninstall or Manage Add-Ons…"
msgstr "Sýsla með eða fjarlægja viðbætur…"

#: plugin/appentry.cpp:319
#, kde-format
msgid "Hide Application"
msgstr "Fela forrit"

#: plugin/appentry.cpp:390
#, kde-format
msgctxt "App name (Generic name)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/appentry.cpp:392
#, kde-format
msgctxt "Generic name (App name)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/appsmodel.cpp:34 plugin/appsmodel.cpp:58 plugin/computermodel.cpp:98
#: plugin/computermodel.cpp:226 plugin/recentusagemodel.cpp:163
#: plugin/recentusagemodel.cpp:226
#, kde-format
msgid "Applications"
msgstr "Forrit"

#: plugin/appsmodel.cpp:151
#, kde-format
msgid "Unhide Applications in this Submenu"
msgstr "Ekki fela forrit í þessari undirvalmynd"

#: plugin/appsmodel.cpp:160
#, kde-format
msgid "Unhide Applications in '%1'"
msgstr "Ekki fela forrit í '%1'"

#: plugin/computermodel.cpp:92
#, kde-format
msgid "Show KRunner"
msgstr "Birta KRunner"

#: plugin/computermodel.cpp:96
#, kde-format
msgid "Search, calculate, or run a command"
msgstr "Leita, reikna eða keyra skipun"

#: plugin/computermodel.cpp:149
#, kde-format
msgid "Computer"
msgstr "Tölva"

#: plugin/contactentry.cpp:106 plugin/recentcontactsmodel.cpp:82
#, kde-format
msgid "Show Contact Information…"
msgstr "Birta upplýsingar um tengilið…"

#. i18n.
#: plugin/kastatsfavoritesmodel.cpp:548 plugin/rootmodel.cpp:416
#: plugin/simplefavoritesmodel.cpp:31
#, kde-format
msgid "Favorites"
msgstr "Eftirlæti"

#: plugin/recentcontactsmodel.cpp:40
#, kde-format
msgid "Contacts"
msgstr "Tengiliðir"

#: plugin/recentcontactsmodel.cpp:73
#, kde-format
msgid "Forget Contact"
msgstr "Gleyma tengilið"

#: plugin/recentcontactsmodel.cpp:77 plugin/recentcontactsmodel.cpp:153
#, kde-format
msgid "Forget All Contacts"
msgstr "Gleyma öllum tengiliðum"

#: plugin/recentusagemodel.cpp:161
#, kde-format
msgid "Recently Used"
msgstr "Nýlega notað"

#: plugin/recentusagemodel.cpp:166 plugin/recentusagemodel.cpp:308
#, kde-format
msgid "Files"
msgstr "Skrár"

#: plugin/recentusagemodel.cpp:248
#, kde-format
msgid "Forget Application"
msgstr "Gleyma forriti"

#: plugin/recentusagemodel.cpp:342
#, kde-format
msgid "Open Containing Folder"
msgstr "Opna umlykjandi möppu"

#: plugin/recentusagemodel.cpp:345
#, kde-format
msgid "Forget File"
msgstr "Gleyma skrá"

#: plugin/recentusagemodel.cpp:495
#, kde-format
msgid "Forget All"
msgstr "Gleyma öllu"

#: plugin/recentusagemodel.cpp:497
#, kde-format
msgid "Forget All Applications"
msgstr "Gleyma öllum forritum"

#: plugin/recentusagemodel.cpp:500
#, kde-format
msgid "Forget All Files"
msgstr "Gleyma öllum skrám"

#: plugin/rootmodel.cpp:95
#, kde-format
msgid "Hide %1"
msgstr "Fela %1"

#: plugin/rootmodel.cpp:398
#, kde-format
msgid "All Applications"
msgstr "Öll forrit"

#: plugin/rootmodel.cpp:410
#, kde-format
msgid "This shouldn't be visible! Use KICKER_FAVORITES_MODEL"
msgstr "Þetta ætti ekki að vera sýnilegt! Notaðu KICKER_FAVORITES_MODEL"

#: plugin/rootmodel.cpp:422
#, kde-format
msgid "Recent Contacts"
msgstr "Nýlegir tengiliðir"

#: plugin/rootmodel.cpp:429
#, kde-format
msgid "Often Used Files"
msgstr "Oft notaðar skrár"

#: plugin/rootmodel.cpp:438
#, kde-format
msgid "Recent Applications"
msgstr "Nýleg forrit"

#: plugin/rootmodel.cpp:438
#, kde-format
msgid "Often Used Applications"
msgstr "Oft notuð forrit"

#: plugin/rootmodel.cpp:453
#, kde-format
msgid "Power / Session"
msgstr "Orka / Seta"

#: plugin/runnermodel.cpp:231
#, kde-format
msgid "Search results"
msgstr "Leitarniðurstöður"

#: plugin/systementry.cpp:195
#, kde-format
msgid "Lock"
msgstr "Læsa"

#: plugin/systementry.cpp:198
#, kde-format
msgid "Log Out"
msgstr "Skrá út"

#: plugin/systementry.cpp:201 plugin/systementry.cpp:269
#, kde-format
msgid "Save Session"
msgstr "Vista setu"

#: plugin/systementry.cpp:204
#, kde-format
msgid "Switch User"
msgstr "Skipta um notanda"

#: plugin/systementry.cpp:207
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Svæfa"

#: plugin/systementry.cpp:210
#, kde-format
msgid "Hibernate"
msgstr "Leggja í dvala"

#: plugin/systementry.cpp:213
#, kde-format
msgid "Restart"
msgstr "Endurræsa"

#: plugin/systementry.cpp:216
#, kde-format
msgid "Shut Down"
msgstr "Slökkva á tölvunni"

#: plugin/systementry.cpp:229 plugin/systementry.cpp:232
#: plugin/systementry.cpp:235 plugin/systementry.cpp:238
#, kde-format
msgid "Session"
msgstr "Seta"

#: plugin/systementry.cpp:241 plugin/systementry.cpp:244
#: plugin/systementry.cpp:247 plugin/systementry.cpp:250
#, kde-format
msgid "System"
msgstr "Kerfi"

#: plugin/systementry.cpp:263
#, kde-format
msgid "Lock screen"
msgstr "Læsa skjá"

#: plugin/systementry.cpp:266
#, kde-format
msgid "End session"
msgstr "Ljúka setu"

#: plugin/systementry.cpp:272
#, kde-format
msgid "Start a parallel session as a different user"
msgstr "Hefja samhliða setu sem annar notandi"

#: plugin/systementry.cpp:275
#, kde-format
msgid "Suspend to RAM"
msgstr "Svæfa í minni"

#: plugin/systementry.cpp:278
#, kde-format
msgid "Suspend to disk"
msgstr "Svæfa á disk"

#: plugin/systementry.cpp:281
#, kde-format
msgid "Restart computer"
msgstr "Endurræsa tölvuna"

#: plugin/systementry.cpp:284
#, kde-format
msgid "Turn off computer"
msgstr "Slökkva á tölvunni"

#: plugin/systemmodel.cpp:31
#, kde-format
msgid "System actions"
msgstr "Kerfisaðgerðir"
