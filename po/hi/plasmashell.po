# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-07 00:49+0000\n"
"PO-Revision-Date: 2021-09-07 10:32+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 21.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "राघवेंद्र कामत"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raghu@raghukamath.com"

#: currentcontainmentactionsmodel.cpp:205
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "माउस क्रिया प्लगइन विन्यस्त करें"

#: main.cpp:88
#, kde-format
msgid "Plasma"
msgstr "प्लाज़्मा"

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "प्लाज़्मा शेल"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "क्यूएमएल जावास्क्रिप्ट डीबगर सक्षम करें"

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "दुर्घटना के बाद प्लाज़्मा-शेल को स्वचालित रूप से पुनरारंभ न करें"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "दिए गए शेल प्लगइन को लोड करने के लिए बाध्य करें"

#: main.cpp:111
#, kde-format
msgid ""
"Load plasmashell as a standalone application, needs the shell-plugin option "
"to be specified"
msgstr ""
"प्लाज़्मा शेल को एक स्वतंत्र अनुप्रयोग के रूप में लोड करें, शेल-प्लगइन विकल्प निर्दिष्ट करने के की "
"आवश्यकता है"

#: main.cpp:113
#, kde-format
msgid "Replace an existing instance"
msgstr "एक वर्तमान चलित अनुप्रयोग को प्रतिस्थापित करें"

#: main.cpp:116
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"परीक्षण मोड को सक्षम बनाता है और परीक्षण वातावरण स्थापित करने के लिए जावास्क्रिप्ट "
"फ़ाइल खाका निर्दिष्ट करता है।"

#: main.cpp:117
#, kde-format
msgid "file"
msgstr "फ़ाइल"

#: main.cpp:121
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "उपयोक्ता प्रतिक्रिया के लिए उपलब्ध विकल्पों की सूची दिखाता है"

#: main.cpp:221
#, kde-format
msgid "Plasma Failed To Start"
msgstr "प्लाज़्मा चालू होने में विफल हुआ"

#: main.cpp:222
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"प्लाज़्मा प्रारंभ होने में असमर्थ है क्योंकि यह ओपनजीएल २ या सॉफ़्टवेयर विकल्प का सही उपयोग "
"नहीं कर सका।\n"
"कृपया जांचें कि आपके ग्राफिक ड्राइवर सही तरीके से स्थापित हैं।"

#: osd.cpp:54
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "ध्वनी बंद है"

#: osd.cpp:72
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "माइक्रोफोन बंद है"

#: osd.cpp:88
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 मूक है"

#: osd.cpp:110
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "स्पर्श-पैड चालू है"

#: osd.cpp:112
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "स्पर्श-पैड बंद है"

#: osd.cpp:119
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "वाइफाइ चालू है"

#: osd.cpp:121
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "वाइफाइ बंद है"

#: osd.cpp:128
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "ब्लूटूथ चालू है"

#: osd.cpp:130
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "ब्लूटूथ बंद है"

#: osd.cpp:137
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "मोबाइल इन्टरनेट चालू है"

#: osd.cpp:139
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "मोबाइल इन्टरनेट बंद है"

#: osd.cpp:147
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "स्क्रीन पर कुंजीपटल सक्रिय"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "स्क्रीन पर कुंजीपटल निष्क्रिय"

#: shellcontainmentconfig.cpp:129
#, kde-format
msgid "Unknown %1"
msgstr ""

#: shellcorona.cpp:175 shellcorona.cpp:177
#, kde-format
msgid "Show Desktop"
msgstr "डेस्कटॉप दिखाएँ"

#: shellcorona.cpp:177
#, kde-format
msgid "Hide Desktop"
msgstr "डेस्कटॉप चिपाएँ"

#: shellcorona.cpp:192
#, kde-format
msgid "Show Activity Switcher"
msgstr "गतिविधि परिवर्तक दिखाएं"

#: shellcorona.cpp:202
#, kde-format
msgid "Stop Current Activity"
msgstr "वर्तमान गतिविधि बंद करें"

#: shellcorona.cpp:209
#, kde-format
msgid "Switch to Previous Activity"
msgstr "पिछली गतिविधि पर जाएँ"

#: shellcorona.cpp:216
#, kde-format
msgid "Switch to Next Activity"
msgstr "अगले गतिविधि पर जाएँ"

#: shellcorona.cpp:231
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "कार्य प्रबंधक प्रविष्टि %1 सक्रिय करें"

#: shellcorona.cpp:257
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr ""

#: shellcorona.cpp:278
#, kde-format
msgid "Move keyboard focus between panels"
msgstr ""

#: shellcorona.cpp:1809
#, kde-format
msgid "Add Panel"
msgstr "पैनल जोडें"

#: shellcorona.cpp:1845
#, fuzzy, kde-format
#| msgid "Empty %1"
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "खाली %1"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "सॉफ्टवेयर रेंडरर जो उपयोग में है"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "सॉफ्टवेयर रेंडरर जो उपयोग में है"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "रेंडरिंग अवक्रमित हो सकता है"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "फिर कभी न दिखाएँ"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "पैनल की गिनती"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "पैनलों को गिनेगा"

#~ msgid "Unable to load script file: %1"
#~ msgstr "स्क्रिप्ट फ़ाइललोड करने में अक्षम : %1"
