# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2019-10-26 14:13+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.2\n"

#: package/contents/ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr ""

#: package/contents/ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr ""

#: package/contents/ui/LocationsFixedView.qml:78
#: package/contents/ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr ""

#: package/contents/ui/LocationsFixedView.qml:222
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""

#: package/contents/ui/LocationsFixedView.qml:235
#, fuzzy, kde-format
#| msgid "Latitude:"
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Latitude:"

#: package/contents/ui/LocationsFixedView.qml:261
#, fuzzy, kde-format
#| msgid "Longitude:"
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Lonxitude:"

#: package/contents/ui/main.qml:87
#, fuzzy, kde-format
#| msgid ""
#| "Night Color makes the colors on the screen warmer to reduce eye strain."
msgid "The blue light filter makes the colors on the screen warmer."
msgstr ""
"A cor de noite fai que as cores da pantalla sexan máis cálidas para reducir "
"o esforzo dos ollos."

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Switching times:"
msgstr ""

#: package/contents/ui/main.qml:100
#, kde-format
msgid "Always off"
msgstr ""

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr ""

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr ""

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Custom times"
msgstr ""

#: package/contents/ui/main.qml:104
#, kde-format
msgid "Always on night color"
msgstr ""

#: package/contents/ui/main.qml:156
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""

#: package/contents/ui/main.qml:175
#, fuzzy, kde-format
#| msgid "Night Color temperature:"
msgid "Day color temperature:"
msgstr "Temperatura da cor de noite:"

#: package/contents/ui/main.qml:218 package/contents/ui/main.qml:277
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr ""

#: package/contents/ui/main.qml:222 package/contents/ui/main.qml:281
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Cálida"

#: package/contents/ui/main.qml:228 package/contents/ui/main.qml:287
#, kde-format
msgctxt "No blue light filter activated"
msgid "Cool (no filter)"
msgstr ""

#: package/contents/ui/main.qml:228 package/contents/ui/main.qml:287
#, kde-format
msgctxt "Night colour blue-ish"
msgid "Cool"
msgstr "Fría"

#: package/contents/ui/main.qml:234
#, fuzzy, kde-format
#| msgid "Night Color temperature:"
msgid "Night color temperature:"
msgstr "Temperatura da cor de noite:"

#: package/contents/ui/main.qml:298
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr ""

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Begin night color at:"
msgstr ""

#: package/contents/ui/main.qml:321 package/contents/ui/main.qml:344
#, kde-format
msgid "Input format: HH:MM"
msgstr "Formato de entrada: HH:MM"

#: package/contents/ui/main.qml:331
#, kde-format
msgid "Begin day color at:"
msgstr ""

#: package/contents/ui/main.qml:353
#, kde-format
msgid "Transition duration:"
msgstr "Duración da transición:"

#: package/contents/ui/main.qml:362
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuto"
msgstr[1] "%1 minutos"

#: package/contents/ui/main.qml:375
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Minutos de entrada (mín. 1, máx. 600)"

#: package/contents/ui/main.qml:393
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Erro: os tempos de transición sobrepóñense."

#: package/contents/ui/main.qml:413
#, fuzzy, kde-format
#| msgid "Location"
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Lugar"

#: package/contents/ui/TimingsView.qml:32
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""

#: package/contents/ui/TimingsView.qml:39
#, fuzzy, kde-format
#| msgid "Night Color"
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr "Cor de noite"

#, fuzzy
#~| msgid "Activate Night Color"
#~ msgid "Activate blue light filter"
#~ msgstr "Activar a cor de noite"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Erro: a mañá vai antes da tarde."

#~ msgid "Detect Location"
#~ msgstr "Detectar a localización"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Adrian Chaves"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "adrian@chaves.io"

#~ msgid "Night Color"
#~ msgstr "Cor de noite"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid " K"
#~ msgstr " K"

#~ msgid "Operation mode:"
#~ msgstr "Modo de funcionamento:"

#~ msgid "Automatic"
#~ msgstr "Automático"

#~ msgid "Times"
#~ msgstr "Horas"

#~ msgid "Constant"
#~ msgstr "Constante"

#~ msgid "Sunrise begins:"
#~ msgstr "Comeza o amencer:"

#~ msgid "(Input format: HH:MM)"
#~ msgstr "(formato de entrada: HH:MM)"

#~ msgid "Sunset begins:"
#~ msgstr "Comeza o ocaso:"

#~ msgid "...and ends:"
#~ msgstr "…e remata:"

#~ msgid "(HH:MM)"
#~ msgstr "(HH:MM)"
