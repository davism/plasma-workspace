# translation of krunner_sessions.po to Russian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Leonid Kanter <leon@asplinux.ru>, 2008.
# Andrey Cherepanov <skull@kde.ru>, 2009.
# Alexander Potashev <aspotashev@gmail.com>, 2010, 2011.
# Alexander Yavorsky <kekcuha@gmail.com>, 2018, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: krunner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-02 00:48+0000\n"
"PO-Revision-Date: 2022-02-06 18:10+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: sessionrunner.cpp:26
#, fuzzy, kde-format
#| msgid "log out"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to log out of the "
"session"
msgid "logout;log out"
msgstr "завершить сеанс"

#: sessionrunner.cpp:29
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Завершить текущий сеанс пользователя"

#: sessionrunner.cpp:36
#, fuzzy, kde-format
#| msgctxt "shut down computer command"
#| msgid "shut down"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to shut down the "
"computer"
msgid "shutdown;shut down"
msgstr "выключить"

#: sessionrunner.cpp:39
#, kde-format
msgid "Turns off the computer"
msgstr "Выключить компьютер"

#: sessionrunner.cpp:46
#, fuzzy, kde-format
#| msgctxt "restart computer command"
#| msgid "restart"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to restart the "
"computer"
msgid "restart;reboot"
msgstr "перезапустить"

#: sessionrunner.cpp:49
#, kde-format
msgid "Reboots the computer"
msgstr "Перезагрузить компьютер"

#: sessionrunner.cpp:57
#, fuzzy, kde-format
#| msgid "Lock the screen"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to lock the screen"
msgid "lock;lock screen"
msgstr "Заблокировать экран"

#: sessionrunner.cpp:59
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Заблокировать текущий сеанс"

#: sessionrunner.cpp:66
#, fuzzy, kde-format
#| msgid "new session"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to save the "
"desktop session"
msgid "save;save session"
msgstr "новый сеанс"

#: sessionrunner.cpp:69
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:76
#, fuzzy, kde-format
#| msgid "switch user"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to switch user "
"sessions"
msgid "switch user;new session"
msgstr "переключить пользователя"

#: sessionrunner.cpp:79
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Начать новый сеанс"

#: sessionrunner.cpp:86
#, fuzzy, kde-format
#| msgctxt "User sessions"
#| msgid "sessions"
msgctxt "KRunner keyword to list user sessions"
msgid "sessions"
msgstr "сеансы"

#: sessionrunner.cpp:87
#, kde-format
msgid "Lists all sessions"
msgstr "Показать все сеансы"

#: sessionrunner.cpp:90
#, fuzzy, kde-format
#| msgctxt "switch user command"
#| msgid "switch"
msgctxt "KRunner keyword to switch user sessions"
msgid "switch"
msgstr "переключить"

#: sessionrunner.cpp:92
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Переключить на сеанс пользователя :q: или показать все активные сеансы, "
"если :q: не указано"

#: sessionrunner.cpp:114
#, kde-format
msgctxt "log out command"
msgid "Log Out"
msgstr ""

#: sessionrunner.cpp:124
#, fuzzy, kde-format
#| msgctxt "shut down computer command"
#| msgid "shut down"
msgctxt "turn off computer command"
msgid "Shut Down"
msgstr "выключить"

#: sessionrunner.cpp:134
#, fuzzy, kde-format
#| msgctxt "restart computer command"
#| msgid "restart"
msgctxt "restart computer command"
msgid "Restart"
msgstr "перезапустить"

#: sessionrunner.cpp:144
#, kde-format
msgctxt "lock screen command"
msgid "Lock"
msgstr ""

#: sessionrunner.cpp:154
#, fuzzy, kde-format
#| msgid "new session"
msgid "Save Session"
msgstr "новый сеанс"

#: sessionrunner.cpp:197
#, kde-format
msgid "Switch User"
msgstr "Переключить пользователя"

#: sessionrunner.cpp:274
#, kde-format
msgid "New Session"
msgstr "Новый сеанс"

#: sessionrunner.cpp:275
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type '%1')</li><li>Plasma widgets (such as the "
"application launcher)</li></ul>"
msgstr ""

#~ msgctxt "log out command"
#~ msgid "logout"
#~ msgstr "завершить сеанс"

#~ msgctxt "lock screen command"
#~ msgid "lock"
#~ msgstr "заблокировать"

#~ msgctxt "restart computer command"
#~ msgid "reboot"
#~ msgstr "перезагрузить"

#~ msgctxt "switch user command"
#~ msgid "switch :q:"
#~ msgstr "переключить :q:"

#~ msgid "new session"
#~ msgstr "новый сеанс"

#~ msgctxt "log out command"
#~ msgid "Logout"
#~ msgstr "Завершить сеанс"

#~ msgid "Restart the computer"
#~ msgstr "Перезагрузить компьютер"

#~ msgctxt "shut down computer command"
#~ msgid "shutdown"
#~ msgstr "выключить"

#~ msgid "Shut down the computer"
#~ msgstr "Выключить компьютер"

#, fuzzy
#~| msgid "new session"
#~ msgid "Save the session"
#~ msgstr "новый сеанс"

#~ msgid "Warning - New Session"
#~ msgstr "Внимание — новый сеанс"

#~ msgid ""
#~ "<p>You have chosen to open another desktop session.<br />The current "
#~ "session will be hidden and a new login screen will be displayed.<br />An "
#~ "F-key is assigned to each session; F%1 is usually assigned to the first "
#~ "session, F%2 to the second session and so on. You can switch between "
#~ "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~ "time. Additionally, the Plasma Panel and Desktop menus have actions for "
#~ "switching between sessions.</p>"
#~ msgstr ""
#~ "<p>Было запрошено открытие нового сеанса.<br />Текущий сеанс будет скрыт, "
#~ "и появится новый экран входа в систему.<br />Каждому сеансу соответствует "
#~ "функциональная клавиша. Как правило, F%1 соответствует первому сеансу, F"
#~ "%2 — второму и так далее. Переключаться между сеансами можно, "
#~ "одновременно нажимая Ctrl, Alt и соответствующую функциональную клавишу. "
#~ "Панель Plasma и меню рабочего стола также позволяют переключаться между "
#~ "сеансами.</p>"

#~ msgid "&Start New Session"
#~ msgstr "&Запустить новый сеанс"
