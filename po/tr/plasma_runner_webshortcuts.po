# translation of krunner_webshortcutsrunner.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Serdar Soytetir <tulliana@gmail.com>, 2008, 2009.
# H. İbrahim Güngör <ibrahim@pardus.org.tr>, 2010.
# Volkan Gezer <volkangezer@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: krunner_webshortcutsrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-01 00:48+0000\n"
"PO-Revision-Date: 2022-02-27 14:13+0100\n"
"Last-Translator: Volkan Gezer <volkangezer@gmail.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#: webshortcutrunner.cpp:44 webshortcutrunner.cpp:148 webshortcutrunner.cpp:165
#, kde-format
msgid "Search %1 for %2"
msgstr "%2 için %1 ara"

#: webshortcutrunner.cpp:72
#, kde-format
msgid "Opens \"%1\" in a web browser with the query :q:."
msgstr "\"%1\" dizgesini :q: sorgusuyla birlikte ağ tarayıcıda açar."

#: webshortcutrunner.cpp:77
#, kde-format
msgid "Search using the DuckDuckGo bang syntax"
msgstr "DuckDuckGo bang sözdizimini kullanarak arama yap"

#: webshortcutrunner.cpp:112
#, kde-format
msgid "Search in private window"
msgstr "Gizli pencerede ara"

#: webshortcutrunner.cpp:112
#, kde-format
msgid "Search in incognito window"
msgstr "Özel pencerede ara"
