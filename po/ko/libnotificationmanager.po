# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Shinjo Park <kde@peremen.name>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-01 00:48+0000\n"
"PO-Revision-Date: 2019-05-18 15:59+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: abstractnotificationsmodel.cpp:293
#, fuzzy, kde-format
#| msgctxt "Copying n files to location"
#| msgid "%1 file to %2"
#| msgid_plural "%1 files to %2"
msgctxt "@info %1 notification body %2 application name"
msgid "%1 from %2"
msgstr "%2(으)로 파일 %1개"

#: job_p.cpp:185
#, kde-format
msgctxt "Copying n of m files to locaton"
msgid "%2 of %1 file to %3"
msgid_plural "%2 of %1 files to %3"
msgstr[0] "%3(으)로 파일 %1개 중 %2개"

#: job_p.cpp:188 job_p.cpp:218 job_p.cpp:234
#, kde-format
msgctxt "Copying n files to location"
msgid "%1 file to %2"
msgid_plural "%1 files to %2"
msgstr[0] "%2(으)로 파일 %1개"

#: job_p.cpp:195
#, kde-format
msgctxt "Copying n of m files"
msgid "%2 of %1 file"
msgid_plural "%2 of %1 files"
msgstr[0] "파일 %1개 중 %2개"

#: job_p.cpp:198 job_p.cpp:228 job_p.cpp:238
#, kde-format
msgctxt "Copying n files"
msgid "%1 file"
msgid_plural "%1 files"
msgstr[0] "파일 %1개"

#: job_p.cpp:203
#, fuzzy, kde-format
#| msgctxt "Copying n of m files"
#| msgid "%2 of %1 file"
#| msgid_plural "%2 of %1 files"
msgctxt "Copying n of m items"
msgid "%2 of %1 item"
msgid_plural "%2 of %1 items"
msgstr[0] "파일 %1개 중 %2개"

#: job_p.cpp:206 job_p.cpp:226
#, kde-format
msgctxt "Copying n items"
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] ""

#: job_p.cpp:213
#, kde-format
msgctxt "Copying file to location"
msgid "%1 to %2"
msgstr "%2(으)로 %1"

#: job_p.cpp:216
#, fuzzy, kde-format
#| msgctxt "Copying n files to location"
#| msgid "%1 file to %2"
#| msgid_plural "%1 files to %2"
msgctxt "Copying n items to location"
msgid "%1 file to %2"
msgid_plural "%1 items to %2"
msgstr[0] "%2(으)로 파일 %1개"

#: job_p.cpp:236
#, kde-format
msgctxt "Copying unknown amount of files to location"
msgid "to %1"
msgstr "%1(으)로"

#: jobsmodel.cpp:109
#, fuzzy, kde-format
#| msgctxt "Copying n files to location"
#| msgid "%1 file to %2"
#| msgid_plural "%1 files to %2"
msgctxt "@info %1 notification body %2 job name"
msgid "%1 from %2"
msgstr "%2(으)로 파일 %1개"

#: jobsmodel_p.cpp:477
#, kde-format
msgid "Application closed unexpectedly."
msgstr "프로그램이 예상하지 못하게 종료되었습니다."
